import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createStackNavigator } from '@react-navigation/stack';
import ButtonCusstom from './ButtonCustom';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';



interface Props{

}
const ComponentButton: React.FC<Props> = ({}) =>{
    return(
        <SafeAreaView style={styles.flexbutton}>
        <View >
        <ButtonCusstom title='Concact' backgroundcolor='#3399FF'
         icon={<Icon name="mobile-alt" size={20}  color={'white'}
       />}   iconbackground='#66CCFF' boderRadius = {false} />
        <ButtonCusstom title='Comment' 
        backgroundcolor='#99CCFF' icon={
            <Icon name="comment-alt" size={20} color={'white'}/>
        } iconbackground='#CCCCCC'/>
        <ButtonCusstom title='DownLoad' backgroundcolor='#FF9900' icon={ 
            <Icon name="download" size={25}  color={'white'}/>
        } iconbackground='#FF9966'/>
        <ButtonCusstom title='Add to card' backgroundcolor='#FFCC33' icon={
        <Icon name="shopping-cart" size={25}  color={'white'}/>}
        iconbackground='#FFCC99'/>
        
        </View>
        <View>
        <ButtonCusstom title='Share' backgroundcolor='#FFCC33' onPress={()=>{alert("abcd")}} icon={
        <Icon name="share-alt" size={20}  color={'white'} />}
        iconbackground='#FF9900' boderRadius={true}/>
         <ButtonCusstom title='Like' backgroundcolor='#FF6600'  icon={
        <Icon name="heart" size={20}  color={'white'}/>}
        iconbackground='#FF3333' boderRadius={true}/>
        <ButtonCusstom title='Upload' backgroundcolor='#99CCFF' icon={
            <Icon name="upload" size={20} color={"white"}/>}
            iconbackground='#CCCCCC' boderRadius={true}/>
         <ButtonCusstom title='Setting' backgroundcolor='#FFCC33' icon={
            <Icon name="upload" size={20} color={"white"}/>
        }iconbackground='#FFCC99' boderRadius={true}/>
        </View>
        </SafeAreaView>
    )
}
const styles=StyleSheet.create({
    flexbutton:{
        display:'flex',
        justifyContent:'center',
        flexDirection:'row',
        alignItems:'center'

    }
})
export default ComponentButton;
