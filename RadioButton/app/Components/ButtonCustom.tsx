import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet,Button,ViewStyle,TextStyle ,ButtonProps} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';  


interface Props extends ButtonProps{
    title : string;
    backgroundcolor: string;
    width?:number;
    height?:number;
    boderRadius?:boolean;
    icon?: JSX.Element;
    iconbackground?:string;
    style?: ViewStyle | TextStyle;
}

const ButtonCustom: React.FC<Props> =({title,backgroundcolor,icon,iconbackground,boderRadius,style, ...rest})=>{
    return(
        <TouchableOpacity style={[styles.container]} {...rest}>
        <View style={[styles.fixtext,{backgroundColor:backgroundcolor,borderRadius:boderRadius ? 50 : 0},style]}>
            <View style={[styles.icon,{backgroundColor:iconbackground,borderRadius:boderRadius ? 50 : 0}]}>
                {icon}
            </View>
             <Text style={{width:'70%'}} >{title}</Text >
             
            </View>
        </TouchableOpacity>
    )
}
const styles=StyleSheet.create({
    container:{
        justifyContent:'center',
        flexDirection:'column',
        alignItems:'center',
        margin:20
    },
    
    fixtext:{
        width:150,
        justifyContent:'center',
        height:30,
        flexDirection:'row',
        backgroundColor:'#0099FF',
        alignItems:'center'
    },
    icon:{
        width:'30%', 
        height:30,
        alignItems:'center',
        justifyContent:"center"
    }
})
export default ButtonCustom;