import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet, Button} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, useNavigation ,getFocusedRouteNameFromRoute} from '@react-navigation/native';
import Screen1 from '../../Main/Screen1';
import Screen2 from '../../Main/Screen2';
import Test2 from '../../Main/Test2';

interface Props{

}

const TestScreen: React.FC<Props> = ({}) =>{
    const Stack= createStackNavigator();
    return(
      
        <Stack.Navigator initialRouteName='screen1'  >
        <Stack.Screen name="screen1" component={Screen1}/>
        <Stack.Screen name="screen2" component={Screen2}/>
        <Stack.Screen name="Test2" component={Test2}/>
    </Stack.Navigator>
       
    )}

export default TestScreen;