import React ,{useState,useRef, useEffect} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute, NavigationContainer } from '@react-navigation/native';
import TestScreen from './app/Components/TestScreen';
import ComponentButton from './app/Components/ComponentButton';
import Test1 from './app/Components/Test1';
import Icon,{Icons} from './Icons/Icon';
import {createDrawerNavigator} from '@react-navigation/drawer';
import * as Animatable from 'react-native-animatable';
import AnimatedTabBar, {TabsConfig, BubbleTabBarItemConfig} from '@gorhom/animated-tabbar';

interface Props{

}

const TabArr = [
  { route: 'Home', 
  label: 'Home', 
  type: Icons.Feather,
   icon: 'home', 
   component: TestScreen,
   color:"#FF9999",
    alphaClr: "#FF9999"},
  { 
    route: 'Search', 
    label: 'Search', 
    type: Icons.Feather, 
    icon: 'search', 
    component: ComponentButton, 
    color: "#FF9999", 
    alphaClr: "#FF9999" },
  { 
    route: 'Add',
     label: 'Add New', 
     type: Icons.Feather, 
     icon: 'plus-square', 
     component: Test1, 
     color: "#FF9999", 
     alphaClr: "#FF9999" },
];

const TabButton=(props)=>{
  const { item, onPress, accessibilityState } = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const textViewRef = useRef(null);

  useEffect(() => {
    if (focused) { // 0.3: { scale: .7 }, 0.5: { scale: .3 }, 0.8: { scale: .7 },
      viewRef.current.animate({ 0: { scale: 0 }, 1: { scale: 1 } });
      textViewRef.current.animate({0: {scale: 0}, 1: {scale: 1}});
    } else {
      viewRef.current.animate({ 0: { scale: 1, }, 1: { scale: 0, } });
      textViewRef.current.animate({0: {scale: 1}, 1: {scale: 0}});
    }
  }, [focused])

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={[styles.container, {flex: focused ? 1 : 0.65}]}>
      <View>
        <Animatable.View
          ref={viewRef}
          style={[StyleSheet.absoluteFillObject, { backgroundColor: item.color, borderRadius: 16 }]} />
        <View style={[styles.btn, { backgroundColor: focused ? null : item.alphaClr }]}>
          <Icon type={item.type} name={item.icon} color={focused ? "#00CC00" : "#FF9999"} />
          <Animatable.View
            ref={textViewRef}>
            {focused && <Text style={{
              color: "#99CCCC", paddingHorizontal: 8
            }}>{item.label}</Text>}
          </Animatable.View>
        </View>
      </View>
    </TouchableOpacity>
  )
}
const Tab= createBottomTabNavigator();

// const Draw= createDrawerNavigator();
const App: React.FC<Props> = ({}) =>{
    return(
      <NavigationContainer>
      <Tab.Navigator
      screenOptions={({route})=>({
        headerShown: false,
        tabBarStyle: {
          display:setvisility(route),
          height: 60,
          position: 'absolute',
          bottom: 16,
          right: 16,
          left: 16,
          borderRadius: 16,
         
        }
      })}
    >
      {TabArr.map((item, index) => {
        return (
          <Tab.Screen key={index} name={item.route} component={item.component}
            options={{
              tabBarShowLabel: false,
              tabBarButton: (props) => <TabButton {...props} item={item} />
            }}
          />
        )
      })}
    </Tab.Navigator>
    </NavigationContainer>
    )}


const styles = StyleSheet.create({
      container: {
        justifyContent: 'center',
        alignItems: 'center',
      },
      btn: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        borderRadius: 16,
      }
    })
const setvisility=route=>{
    const routeName = getFocusedRouteNameFromRoute(route);
    // console.log(routeName);
    switch(routeName){
        case 'screen2':
            return 'none';
    return 'flex';    
    }
}
export default App;