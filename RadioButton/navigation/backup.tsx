import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute, NavigationContainer } from '@react-navigation/native';
import TestScreen from './app/Components/TestScreen';
import ComponentButton from './app/Components/ComponentButton';
import Test1 from './app/Components/Test1';
import {createDrawerNavigator} from '@react-navigation/drawer'
import AnimatedTabBar, {TabsConfig, BubbleTabBarItemConfig} from '@gorhom/animated-tabbar';

interface Props{

}

  
const Tab= createBottomTabNavigator();

// const Draw= createDrawerNavigator();
const App: React.FC<Props> = ({}) =>{
    return(
        <NavigationContainer>
        <Tab.Navigator 
        
        screenOptions={({route})=>({
            tabBarActiveTintColor:'#FF6666',
            // tabBarInactiveBackgroundColor:'#99FFFF',
            tabBarInactiveTintColor:'gray',
            headerStyle:{
                backgroundColor:'#FF9999',
                
            },
            headerTitleStyle:{
                color:'white',
                alignItems:'center',
                justifyContent:'center'
            },
            tabBarStyle:{
                height:60,
                position:'absolute',
                bottom:16,
                right:16,
                left:16,
                borderRadius:10,
                display:setvisility(route)
            },
            
        })}  >
            <Tab.Screen  name ='Home 'component={TestScreen}
             options={({route})=>({
               
                tabBarIcon:({color,size})=>{
                return <Icon name='home' color={color} size={size}/>
                },
                headerShown:false,
                
            })}
            />
            <Tab.Screen name ="Heart" component={ComponentButton}
            options={({route})=>({
                tabBarIcon:({color,size})=>{
                return <Icon name ='heart' color={color} size={size}/>
                }
            })}
            />
            <Tab.Screen name="Book" component={Test1}
            options={{
                tabBarIcon:({color,size})=>{
                return <Icon name="book-open" color={color} size={size}/>
                }
            }}/>
        </Tab.Navigator>
        {/* <Draw.Navigator>
            <Draw.Screen name="Home" component={TestScreen}/>
            <Draw.Screen name="Heart" component={ComponentButton}/>
            <Draw.Screen name='Book' component={Test1}/>
        </Draw.Navigator> */}
        </NavigationContainer>
    )}
const setvisility=route=>{
    const routeName = getFocusedRouteNameFromRoute(route);
    // console.log(routeName);
    switch(routeName){
        case 'screen2':
            return 'none';
    return 'flex';    
    }
}
export default App;