import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet, Button} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createStackNavigator } from '@react-navigation/stack';
import { RadioButton } from 'react-native-paper';


interface Props{

}

const Screen2: React.FC<Props> = ({}) =>{
    const [checked, setChecked] = React.useState('first');
    return(
        <View>
        <View style={styles.radio}>
            <Text>Nomal</Text>
        <RadioButton
          value="first"
          
          status={ checked === 'first' ? 'checked' : 'unchecked' }
          onPress={() => setChecked('first')}
        />
        </View>
        <View style={styles.radio}>
            <Text>Custom</Text>
        <RadioButton
          value="second"
          color="red"
          status={ checked === 'second' ? 'checked' : 'unchecked' }
          onPress={() => setChecked('second')}
        />
        </View>
      </View>
    );}
const styles=StyleSheet.create({
    radio:{
        flexDirection:'row',
        alignItems:'center'
    }
})
export default Screen2;