import React ,{useState} from 'react';
import {SafeAreaView, View, Text, FlatList, TouchableOpacity,StyleSheet, Button} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, useNavigation } from '@react-navigation/native';


interface Props{

}

const Screen1: React.FC<Props> = ({}) =>{
    const navigation=useNavigation();
    return(
       <View>
           <Text>Main 1 Screen</Text>
           <Button title='Detail'
           onPress={()=>
           navigation.navigate('screen2')}/>
       </View>
    )}

export default Screen1;